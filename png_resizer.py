try:
    import sys
    from tqdm import tqdm
    import numpy as np
    import glob
    import cv2
    import os
except ImportError:
    print('Dependencies are not installed, please intall opencv-python and tqdm packages.')

def resize_images(path, out_path):
    pbar = tqdm(glob.glob(f'{path}/*.png'))

    for imgfile in pbar:
        pbar.set_description(desc=f'Processing {imgfile}')
    
        img = cv2.imread(imgfile, cv2.IMREAD_UNCHANGED)
        (height, width, _) = img.shape

        new_height = height if height / 4 == height // 4 else (height // 4 + 1) * 4
        new_width = width if width / 4 == width // 4 else (width // 4 + 1) * 4

        height_diff = new_height - height
        width_diff = new_width - width

        if height_diff > 0 or width_diff > 0:
            new_img = cv2.copyMakeBorder(
                img,
                height_diff // 2, height_diff - height_diff // 2, width_diff // 2, width_diff - width_diff // 2,
                cv2.BORDER_CONSTANT,
                value=[0,0,0,0]
            )

            cv2.imwrite(f"{out_path}/{imgfile.split('/')[-1].replace('.png', '-edited.png')}", new_img)


def main():
    if len(sys.argv) < 2:
        print('No argument supplied in the arguments')    
        return

    path = sys.argv[1]
  
    if not os.path.isdir(path):
        print('Given path does not exist')
        return

    out_path = f'{path}/edited'

    if os.path.exists(out_path):
        proceed = input('Edited folder already exists, do you want to override? (y/n)')
        if proceed != 'y':
            return
    else:
        os.makedirs(out_path)

    resize_images(path, out_path)

    print(f'The edited images are in {out_path}')

if __name__ == '__main__':
  main()

