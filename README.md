# PNG Resizer
This script is a tool for resizing groups of PNG images so that their shape is devidable by four, so that specific compression methods in Unity Game Engine can be applied to them.

## How it works
The process is simple. This script checks the shape of each given image and if its width and height are not multiples of 4, it will pad the image with transparent pixels.

Pay attention that the resulting image is not necesserily at the center, padding calculation is as follows:

```python
top     = height_diff // 2
bottom  = height_diff - height_diff // 2
left    = width_diff // 2
right   = width_diff - width_diff // 2
```

## Dependencies
This tool uses the following libraries:
- OPENCV
- TQDM

In order to install them please run:

```
pip install opencv-python tqdm
```

## Usage
Put all the PNG images that you want to transform in a `folder` and run the script as instructed below:

```
python png_resizer.py ${folder}
```
